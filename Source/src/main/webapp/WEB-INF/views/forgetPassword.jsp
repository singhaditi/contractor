<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
     pageEncoding="ISO-8859-1" isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Forgot Password</title>
</head>
<body>

     <h1>Forgot Password</h1>
     <form:form  method="post" modelAttribute = "register" >
          <br>
          <br>
          <table>
				<tr>
				<td colspan="4"><p style="color:red">${error}</p></td>
				</tr>
              <tr>
                   <td>User Id:</td>
                   <td><sf:input type="text" path="userId"
                             required="required" /></td>
                   
              </tr> 

              <tr>
                   <td>Question</td>
                   <td><sf:select path="questionType" required="required">
                             <sf:option value="NONE" label="Select" />
                             <sf:options items="${questionList}" />
                        </sf:select></td>

              </tr>
              <tr>
                   <td>Answer:</td>
                   <td><sf:input type="text" path="answer" required="required" /></td>
              </tr>
          </table>
          <br>
          <br>
          <input type="submit" value="Submit" />
     </form:form>

</body>
</html>
