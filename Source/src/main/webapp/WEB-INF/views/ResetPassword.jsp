<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Forgot Password</title>
</head>
<body>

	<h1>Forgot Password</h1>
	<form:form method="post" modelAttribute="reset">
		<br>
		<br>

		<table>
			<tr>
				<td colspan="3"><p style="color: red">${error}</p></td>
			</tr>
			<tr>
				<td><form:label path="newpassword">Enter Password:</form:label></td>
				<td><form:input path="newpassword" id="newpassword" /></td>
			</tr>

			<tr>
				<td><form:label path="repassword">Retype Password:</form:label></td>
				<td><form:input path="repassword" id="repassword" /></td>

			</tr>

		</table>
		<br>
		<br>
		<input type="submit" value="Submit" />
	</form:form>

</body>
</html>