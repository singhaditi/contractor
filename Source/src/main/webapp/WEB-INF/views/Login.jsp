<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>


<html>

<head>
<!--  <center><div bg-color="blue"><font color="white" >Contractor Hiring Management System</font></div></center>
		-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contractor Hiring</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" type="text/css" media="all">
    <link rel="stylesheet" href="https://bootstrapmade.com/demo/assets/css/normalize.css">
    <link rel="stylesheet" href="https://bootstrapmade.com/demo/assets/css/fontello.css">
    <link rel="stylesheet" href="https://bootstrapmade.com/demo/assets/css/style.css">

<style>

.container {

width: 500px;
  border-radius: 5px;
  background-color: white;
  padding: 20px;
}

body {
	 background-image: url("../images/contractorlogin.jpg");
	 background-repeat: no-repeat;
background-attachment: fixed;
  background-size: cover;
	 background-color: "lavender";
	
}

</style>
</head>

<body style="background-color:lavender">
<h1><center><font color="white"> Login Form</font> </center></h1>


<form:form action="/login" method="post" modelAttribute = "login">
	<center><table>
			<tr><td><center>
			<div class="container">
              <table>
             
				<td><form:label path="userId">User Id</form:label></td>
				<td><form:input path="userId" id="userId" required="required"/></td>
			</tr>

		
			
			<tr>
				<td><form:label path="Password">Password</form:label></td>
				<td><form:password path="Password" id="password" required="required"/></td>
			</tr>

			<tr>
				<td><form:label path="userType">User Type</form:label></td>
				<td><form:radiobuttons path="userType" items="${usertype}"/></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit" id="submit" name="submit"></td>
				<td><form:button type="button"><a href="/Registration">New User? Register</a></form:button></td>
				<td><a href="/ForgetPassword">Forget Password</a></td> 
			</tr>
		</table>
		</div>
	</center></td></tr>
	<tr><td>
	<center><div class="container"><img src="../images/hero-img.png" width="300" height="300" /></div></center>
		</td></tr>
	</table></center>
</form:form>

</body>
</html>	 