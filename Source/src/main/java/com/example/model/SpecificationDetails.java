package com.example.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class SpecificationDetails {
//        @Min(value=1000, message="must be greater than 1000")
//	@Max(value=1000000, message="must be less than 1000000")
    @NotEmpty(message="Please fill valid input.")
	private int budget;
//
//        @Min(value=1, message="must be greater than 1")
//	@Max(value=80, message="must be less than 80")
    @NotEmpty(message="Please fill valid input.")
	private int duration;

    @NotNull
	private String services;
//
//        @Min(value=1, message="must be greater than 1")
//	@Max(value=18, message="must be less than 18")
    @NotEmpty(message="Please fill valid input.")
	private int hours;
        
	private String userid;

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
